# فهرست

* [روی جلد](README.md)
* [دریافت کتاب](download.md)
* [حمایت از پروژه‌](donate.md)

### کتاب
* [۱. مقدمه](book/README.md)
* [۲. معرفی اوبونتو](book/introduction.md)
* [۳. نصب اوبونتو](book/installation.md)
* [۴. شروع کار با GNOME](book/gnome.md)
* [۵. کارهای بعد از نصب](book/post-install.md)
* [۶. نصب نرم‌افزار](book/software-install.md)
* [۷. نرم‌افزارهای اوبونتو](book/ubuntu-software.md)
* [۸. کار با ترمینال](book/terminal.md)
* [۹. ادامه‌ی راه](book/whats-next.md)